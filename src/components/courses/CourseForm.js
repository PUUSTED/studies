import React from 'react';
import PropTypes from 'prop-types';
import TextInput from '../common/TextInput';
import SelectInput from '../common/SelectInput';

const CourseForm = ({
  course,
  authors,
  onSave,
  onChange,
  saving = false,
  errors = {},
}) => {
  return (
    <form id="form" onSubmit={onSave}>
      <h2 className="text-color-main">{course.id ? 'Edit' : 'Add'} Course</h2>
      {errors.onSave && (
        <div className="error-box" role="alert">
          {errors.onSave}
        </div>
      )}
      <TextInput
        name="title"
        label="Title"
        value={course.title}
        onChange={onChange}
        error={errors.title && (
          <div className="error-box" role="alert">
            {errors.title}
          </div>
        )}
      />

      <SelectInput
        name="authorId"
        label="Author"
        value={course.authorId || ''}
        defaultOption="Select Author"
        options={authors.map(author => ({
          value: author.id,
          text: author.name,
        }))}
        onChange={onChange}
        error={errors.author && (
          <div className="error-box" role="alert">
            {errors.author}
          </div>
        )}
      />

      <TextInput
        name="category"
        label="Category"
        value={course.category}
        onChange={onChange}
        error={errors.category && (
          <div className="error-box" role="alert">
            {errors.category}
          </div>
        )}
      />

      <button type="submit" disabled={saving} className="input-save">
        {saving ? 'Saving...' : 'Save'}
      </button>
    </form>
  );
};

CourseForm.propTypes = {
  authors: PropTypes.array.isRequired,
  course: PropTypes.object.isRequired,
  errors: PropTypes.object,
  onSave: PropTypes.func.isRequired,
  onChange: PropTypes.func.isRequired,
  saving: PropTypes.bool,
};

export default CourseForm;
