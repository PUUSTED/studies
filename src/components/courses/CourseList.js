import React, {useState, useEffect} from 'react';
import PropTypes from 'prop-types';
import { Link } from 'react-router-dom';
import ReactPaginate from 'react-paginate';

const ITEMS_PER_PAGE = 5;

const CourseList = ({ courses, onDeleteClick }) => {
const [currentCourses, setCurrentCourses] = useState(null);
const [allCourses, setAllCourses] = useState([]);
const [pageCount, setPageCount] = useState(1);
const [itemOffset, setItemOffset] = useState(0);
const [sortField, setSortField] = useState("");
const [order, setOrder] = useState("asc");

const columns = [
  { label: "Title Name", accessor: "title", sortable: true },
  { label: "Author", accessor: "authorId", sortable: true },
  { label: "Category", accessor: "category", sortable: true },
 ];

 useEffect(() => {
  if (courses) {
    setAllCourses(courses);
  }
 }, [courses]);

useEffect(() => {
  // Fetch items from another resources.
  const endOffset = itemOffset + ITEMS_PER_PAGE;
  console.log(`Loading items from ${itemOffset} to ${endOffset}`);
  setCurrentCourses(allCourses.slice(itemOffset, endOffset));
  setPageCount(Math.ceil(allCourses.length / ITEMS_PER_PAGE));
}, [itemOffset, allCourses, ITEMS_PER_PAGE]);

const handlePageClick = (event) => {
  const newOffset = (event.selected * ITEMS_PER_PAGE) % allCourses.length;
  console.log(
    `User requested page number ${event.selected}, which is offset ${newOffset}`
  );
  setItemOffset(newOffset);
};

const handleSortingChange = (accessor) => {
  const sortOrder =
    accessor === sortField && order === "asc" ? "desc" : "asc";
  setSortField(accessor);
  setOrder(sortOrder);
  handleSorting(accessor, sortOrder);
};

const handleSorting = (sortField, sortOrder) => {
  if (sortField) {
    const sorted = [...allCourses].sort((a, b) => {
      if (a[sortField] === null) return 1;
      if (b[sortField] === null) return -1;
      if (a[sortField] === null && b[sortField] === null) return 0;
      return (
        a[sortField].toString().localeCompare(b[sortField].toString(), "en", {
          numeric: true,
        }) * (sortOrder === "asc" ? 1 : -1)
      );
    });
    setAllCourses(sorted);
  }
};

return (
  <>
  <table className="table">
    <thead>
      <tr>
        <th/>
        {columns.map(({ label, accessor, sortable }) => {
          const cl = sortable
            ? sortField && sortField === accessor && order === "asc"
              ? "up"
              : sortField && sortField === accessor && order === "desc"
              ? "down"
              : "default"
            : "";
          return (
            <th
              key={accessor}
              onClick={sortable ? () => handleSortingChange(accessor) : null}
              className={cl}
            >
              {label}
            </th>
          );
        })}
        <th/>
      </tr>
    </thead>
    <tbody>
      {currentCourses?.map((course) => {
        return (
          <tr key={course.id}>
            <td>
              <a
                className="link-watch"
                href={'http://pluralsight.com/courses/' + course.slug}
              >
                Watch
              </a>
            </td>
            <td className="custom-td">
              <Link to={'/course/' + course.slug} className="link-courses">
                {course.title}
              </Link>
            </td>
            <td className="custom-td">{course.authorName}</td>
            <td className="custom-td">{course.category}</td>
            <td>
              <button
                className='button-delete'
                onClick={() => onDeleteClick(course)}
              >
                Delete
              </button>
            </td>
          </tr>
        );
      })}
    </tbody>
  </table>
    <ReactPaginate
      previousLabel={'<='}
      nextLabel={'=>'}
      breakLabel={'..'}
      pageCount={pageCount}
      marginPagesDisplayed={2}
      pageRangeDisplayed={3}
      onPageChange={handlePageClick}
      containerClassName={'paginator'}
    />
  </>
)};

CourseList.propTypes = {
  courses: PropTypes.array.isRequired,
  onDeleteClick: PropTypes.func.isRequired,
};

export default CourseList;
