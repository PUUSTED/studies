import React from 'react';

const AboutPage = () => (
  <div>
    <h2 className="text-color-main">About</h2>
    <p className="text-color-main">
      This app uses React, Redux, React Router, and many other helpful libraries
    </p>
  </div>
);

export default AboutPage;
