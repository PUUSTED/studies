import React from 'react';
import { Link } from 'react-router-dom';

const HomePage = () => (
  <div>
    <h1 className="text-color-main">Pluralsight Administration</h1>
    <p className="text-color-main">
      React, Redux and React Router for ultra-responsive web apps.
    </p>
    <Link to="about" className="link-color link-position">
      Learn more
    </Link>
  </div>
);

export default HomePage;
