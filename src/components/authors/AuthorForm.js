import React from 'react';
import PropTypes from 'prop-types';
import TextInput from '../common/TextInput';

const AuthorForm = ({
  author,
  onSave,
  onChange,
  saving = false,
  errors = {},
}) => {
  return (
    <form id="form" onSubmit={onSave}>
      <h2 className="text-color-main">{author.id ? 'Edit' : 'Add'} Author</h2>
      {errors.onSave && (
        <div className="error-box" role="alert">
          {errors.onSave}
        </div>
      )}
      <TextInput
        name="name"
        label="Name"
        value={author.name}
        onChange={onChange}
        error={errors.name && (
          <div className="error-box" role="alert">
            {errors.name}
          </div>
        )}
      />

      <button type="submit" disabled={saving} className="input-save">
        {saving ? 'Saving...' : 'Save'}
      </button>
    </form>
  );
};

AuthorForm.propTypes = {
  author: PropTypes.object.isRequired,
  errors: PropTypes.object,
  onSave: PropTypes.func.isRequired,
  onChange: PropTypes.func.isRequired,
  saving: PropTypes.bool,
};

export default AuthorForm;
