import React, { useEffect, useState } from 'react';
import { connect } from 'react-redux';
import { loadAuthors, saveAuthor } from '../../redux/actions/authorActions';
import PropTypes from 'prop-types';
import AuthorForm from './AuthorForm';
import { newAuthor } from '../../../tools/mockData';
import Spinner from '../common/Spinner';
import { toast } from 'react-toastify';

export function ManageAuthorPage({
  // internal note: function properties take precedence over the object properties
  // internal note: these object are in a deconstructor
  authors,
  loadAuthors,
  saveAuthor,
  history,
  ...props
}) {
  const [author, setAuthor] = useState({ ...props.author });
  const [errors, setErrors] = useState({});
  const [saving, setSaving] = useState(false);
  useEffect(() => {
    if (authors.length === 0) {
      loadAuthors().catch((error) => {
        alert('Loading authors failed' + error);
      });
    } else {
      setAuthor({ ...props.author });
    }

  }, [props.author]);

  function handleChange(event) {
    const { name, value } = event.target;
    setAuthor(prevAuthor => ({
      ...prevAuthor,
      [name]: name === 'id' ? parseInt(value, 10) : value,
    }));
  }

  function formIsValid() {
    const { name } = author;
    const errors = {};

    if (!name) errors.name = 'Author is required.';

    setErrors(errors);
    return Object.keys(errors).length === 0;
  }

  function handleSave(event) {
    event.preventDefault();
    if (!formIsValid()) { 
      return;
    } else {
    setSaving(true);
    saveAuthor(author)
      .then(() => {
        toast.success('Author saved.');
        history.push('/authors');
      })
      .catch(error => {
        setSaving(false);
        setErrors({ onSave: error.message });
      });
    }
  }

  return authors.length === 0 ? (
    <Spinner />
  ) : (
    <AuthorForm
      author={author}
      errors={errors}
      authors={authors}
      onChange={handleChange}
      onSave={handleSave}
      saving={saving}
    />
  );
}

ManageAuthorPage.propTypes = {
  author: PropTypes.object,
  authors: PropTypes.array.isRequired,
  loadAuthors: PropTypes.func.isRequired,
  saveAuthor: PropTypes.func.isRequired,
  // history object is from React Router <Route>, any object passed trough Route gets the history object automatically
  history: PropTypes.object.isRequired
};

ManageAuthorPage.defaultProps = {
  author: undefined
}

export function getAuthorBySlug(authors, slug) {
  return authors.find(author => author.slug === slug) || null;
}

export function getCourseBySlug(courses, slug) {
  return courses.find(course => course.slug === slug) || null;
}

function mapStateToProps(state, ownProps) {
  const slug = ownProps.match.params.slug;
  console.log(ownProps, "name stuff");
  console.log(state.authors, "state authors");
  const author =
    slug && state.authors.length > 0
    ? getAuthorBySlug(state.authors, slug)
    : newAuthor;
  const course =
    slug && state.courses.length > 0
    ? getCourseBySlug(state.courses, slug)
    : newAuthor;
  console.log(course, "return courses");
  return {
    author,
    course,
    authors: state.authors,
  };
}

// internal note: this object is avoiding same name conflicts because ManageAuthorPage is an object
const mapDispatchToProps = {
  loadAuthors,
  saveAuthor,
};

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(ManageAuthorPage);
