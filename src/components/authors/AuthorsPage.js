import React from 'react';
import { connect } from 'react-redux';
import * as authorActions from '../../redux/actions/authorActions';
import PropTypes from 'prop-types';
import { bindActionCreators } from 'redux';
import AuthorList from './AuthorList';
import { Redirect } from 'react-router-dom';
import Spinner from '../common/Spinner';
import { toast } from 'react-toastify';

class AuthorsPage extends React.Component {
  // class field to save some typing
  state = {
    redirectToAddAuthorsPage: false
  };

  componentDidMount() {
    const { authors, actions } = this.props;

    if (authors.length === 0) {
      actions.loadAuthors().catch((error) => {
        alert('Loading authors failed' + error);
      });
    }
  }

  handleDeleteAuthor = async author => {
    toast.success('Authors deleted.');
    try {
      this.props.actions.deleteAuthor(author);
    } catch(error) {
      toast.error('Delete failed.' + error.message, { autoClose: false })
    }
  };

  render() {
    return (
      <>
        {this.state.redirectToAddAuthorPage && <Redirect to='/author' />}
        <h2 className="text-color-main">Authors</h2>
        {this.props.loading ? (
          <Spinner /> 
        ) : (
          <>
            <button 
              className='button-custom button-nav'
              onClick={() => this.setState({ redirectToAddAuthorPage: true })}
            >
              Add Author
            </button>

            <AuthorList 
              onDeleteClick={this.handleDeleteAuthor} 
              authors={this.props.authors} 
            />
          </>
        )}
      </>
    );
  }
}

AuthorsPage.propTypes = {
  authors: PropTypes.array.isRequired,
  actions: PropTypes.object.isRequired,
  loading: PropTypes.bool.isRequired,
};

function mapStateToProps(state) {
  return {
    authors:
      state.authors.length === 0
        ? []
        : state.authors.map((author) => {
            return {
              ...author,
              authorName: state.authors.find((a) => a.id === author.id)
                .name,
            };
          }),
    loading: state.apiCallsInProgress > 0
  };
}

function mapDispatchToProps(dispatch) {
  return {
    actions: {
      loadAuthors: bindActionCreators(authorActions.loadAuthors, dispatch),
      deleteAuthor: bindActionCreators(authorActions.deleteAuthor, dispatch),
    },
  };
}

export default connect(mapStateToProps, mapDispatchToProps)(AuthorsPage);
