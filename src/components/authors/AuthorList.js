import React, {useState, useEffect} from 'react';
import PropTypes from 'prop-types';
import { Link } from 'react-router-dom';
import Paginator from '../common/Paginator';

const ITEMS_PER_PAGE = 5;

const AuthorList = ({ authors, onDeleteClick, onChange}) => {
const [currentAuthors, setCurrentAuthors] = useState(null);
const [pageCount, setPageCount] = useState(1);
const [itemOffset, setItemOffset] = useState(0);

useEffect(() => {
  // Fetch items from another resources.
  const endOffset = itemOffset + ITEMS_PER_PAGE;
  console.log(`Loading items from ${itemOffset} to ${endOffset}`);
  setCurrentAuthors(authors.slice(itemOffset, endOffset));
  setPageCount(Math.ceil(authors.length / ITEMS_PER_PAGE));
}, [itemOffset, authors, ITEMS_PER_PAGE]);

const handlePageClick = (event) => {
  const newOffset = (event.selected * ITEMS_PER_PAGE) % authors.length;
  console.log(
    `User requested page number ${event.selected}, which is offset ${newOffset}`
  );
  setItemOffset(newOffset);
};

return (
  <>
  <table className="table">
    <thead>
      <tr>
        <th className="custom-th">Author Name</th>
        <th className="custom-th" />
      </tr>
    </thead>
    <tbody>
      {currentAuthors?.map((author) => {
        return (
          <tr key={author.id}>
            <td className="custom-td">
              <Link to={'/author/' + author.slug} className="link-courses">
                {author.name}
              </Link>
            </td>
            <td>
              <button
                className='button-delete'
                onClick={() => onDeleteClick(author)}
                disabled={author.hasCourses}
                onChange={onChange}
              >
                Delete
              </button>
            </td>
          </tr>
        );
      })}
    </tbody>
  </table>
  <Paginator pageCount={pageCount} handlePageClick={handlePageClick}/>
  </>
)};

AuthorList.propTypes = {
  authors: PropTypes.array.isRequired,
  onDeleteClick: PropTypes.func.isRequired,
};

export default AuthorList;
