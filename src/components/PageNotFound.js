import React from 'react';

const PageNotFound = () => (
  <h1 className="text-color-passive">
    We did our best, but could not find the page. Please accept our sincere
    apologies for your inconvenience.
  </h1>
);

export default PageNotFound;
