import React from 'react';
import ReactPaginate from 'react-paginate';

// const ITEMS_PER_PAGE = 5;

// const DetailList = ({ details, onDeleteClick }) => {
// const [currentDetails, setCurrentDetails] = useState(null);
// const [pageCount, setPageCount] = useState(1);
// const [itemOffset, setItemOffset] = useState(0);

// useEffect(() => {
//   // Fetch items from another resources.
//   const endOffset = itemOffset + ITEMS_PER_PAGE;
//   console.log(`Loading items from ${itemOffset} to ${endOffset}`);
//   setCurrentDetails(details.slice(itemOffset, endOffset));
//   setPageCount(Math.ceil(details.length / ITEMS_PER_PAGE));
// }, [itemOffset, details, ITEMS_PER_PAGE]);

// const handlePageClick = (event) => {
//   const newOffset = (event.selected * ITEMS_PER_PAGE) % details.length;
//   console.log(
//     `User requested page number ${event.selected}, which is offset ${newOffset}`
//   );
//   setItemOffset(newOffset);
// };

const Paginator = ({ pageCount, handlePageClick }) => {
  return (
    <>
      <ReactPaginate
        previousLabel={'<='}
        nextLabel={'=>'}
        breakLabel={'..'}
        pageCount={pageCount}
        marginPagesDisplayed={2}
        pageRangeDisplayed={3}
        onPageChange={handlePageClick}
        containerClassName={'paginator'}
      />
    </>
  )};
// };

// DetailList.propTypes = {
//   Details: PropTypes.array.isRequired,
//   onDeleteClick: PropTypes.func.isRequired,
// };

export default Paginator;