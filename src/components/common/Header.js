import React from 'react';
import { NavLink } from 'react-router-dom';

const Header = () => {
  const activeStyle = { color: '#FF9494' };

  return (
    <nav>
      <NavLink to="/" activeStyle={activeStyle} className="link-color" exact>
        Home
      </NavLink>
      <hr className="hr-vertical" />
      <NavLink to="/courses" activeStyle={activeStyle} className="link-color">
        Courses
      </NavLink>
      <hr className="hr-vertical" />
      <NavLink to="/authors" activeStyle={activeStyle} className="link-color">
        Authors
      </NavLink>
      <hr className="hr-vertical" />
      <NavLink to="/about" activeStyle={activeStyle} className="link-color">
        About
      </NavLink>
    </nav>
  );
};

export default Header;
